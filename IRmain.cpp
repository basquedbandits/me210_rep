#include <Arduino.h>
#include <IntervalTimer.h>
// Create an IntervalTimer object 
IntervalTimer outputTimer;
IntervalTimer freqTimer;


#define PIN_SIGNAL_IN   2

const int ledPin = 13; // pin with the LED

const int IR_Pin = 5;  // the pin with a LED

// Timer functions
void fun_HI_LO();
void fun_freq_read_print();

volatile int print_freq = 100; // Hz

int mapPotentio();
void CountFallingEdges();
volatile int counter = 0;

volatile int current_freq = 1000;

int target_IR_freq = 1000;

int target_freq_counter = 0;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(IR_Pin, OUTPUT);
  Serial.begin(9600);
  outputTimer.begin(fun_HI_LO, 1000000/1250/2);
  freqTimer.begin(fun_freq_read_print,1000000/print_freq);

  attachInterrupt(digitalPinToInterrupt(PIN_SIGNAL_IN), CountFallingEdges, FALLING);

}

// The interrupt will blink the LED, and keep
// track of how many times it has blinked.
int ledState = LOW;
//volatile unsigned long blinkCount = 0; // use volatile for shared variables

// functions called by IntervalTimer should be short, run as quickly as
// possible, and should avoid calling other functions if possible.



void CountFallingEdges(){
  counter ++;
}

void fun_freq_read_print() {
  Serial.println(counter*print_freq);

  current_freq = counter*print_freq;

  counter = 0;
}

void fun_HI_LO() {
  if (ledState == LOW) {
    ledState = HIGH;
    
  } else {
    ledState = LOW;
  }
  digitalWrite(IR_Pin, ledState);
}

int mapPotentio(){
  int sensorValue = analogRead(A1); //read the input on analog pin A1 (15)

  int mappedValue = map(sensorValue, 0, 1023, 50, 12500); // Map between 50Hz and 12500Hz
 
  // Serial.println(mappedValue); //print out the value you read
  
  return mappedValue;
}




// The main program will print the blink count
// to the Arduino Serial Monitor
void loop() {
  
  //unsigned long countCopy;  // holds a copy of the counter
   
   int freq_mapped = mapPotentio();
   outputTimer.update(1000000/freq_mapped/2);


   if (current_freq > target_IR_freq -50 && current_freq < target_IR_freq + 50){
     target_freq_counter ++;
   }

  // Turn LED on if target frequency is found
   if (target_freq_counter > 100){
     digitalWrite(ledPin, HIGH);
     delay(100);
     digitalWrite(ledPin, LOW);
     target_freq_counter = 0;
     
   }else{
     digitalWrite(ledPin, LOW);
   }

   Serial.print(target_freq_counter);
   
  

  // to read a variable which the interrupt code writes, we
  // must temporarily disable interrupts, to be sure it will
  // not change while we are reading.  To minimize the time
  // with interrupts off, just quickly make a copy, and then
  // use the copy while allowing the interrupt to keep working.
  //noInterrupts();
  //fun_freq_read_print();
  //interrupts();

}
