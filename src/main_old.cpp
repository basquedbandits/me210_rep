#include <Arduino.h>
#include <Metro.h>
#include <IntervalTimer.h>

// Create IntervalTimer for IR sensing 
IntervalTimer IRfreqTimer;

//Motor Enable Pins
#define RIGHT_MOTOR A3
#define LEFT_MOTOR A2

//IR Input Pin
#define PIN_SIGNAL_IN   2

//Tape Sensor Input Pins
#define FRONT_TAPE_PIN A6

#define RIGHT_TAPE_PIN A4 
#define LEFT_TAPE_PIN A5 
#define BACK_TAPE_PIN A6 
#define MID_TAPE_PIN A7 




//Light Threshold Level (number increases with darker surfaces)
#define LIGHT_THRESHOLD 500 

#define MAX_SPEED 70
#define SLOWER_SPEED 60
#define SPIN_SPEED 150

//Function Definitions
void find_beacon(void);
void find_line(void);
void position_on_line(void);
void move_backward(void);
void move_forward(void);
void wedge_mode(void);
void checkGlobalEvents(void);

//GlobalEvents functions
int backwards_switch(void);
void checkFrontTape(void);
void checkMidTape(void);
void checkLeftTape(void);
void checkRightTape(void);
void checkBackTape(void);

//Other Functions
void resetTapeSensors(void);
void toggleRightMotorDirection(void);
void toggleLeftMotorDirection(void);
void CountFallingEdges(void);
void IRfreqRead(void);
int checkbeacon(void);

//State Definitions
typedef enum
{
	STATE_PLANT_WEDGE,
	STATE_UNDERWAY,

	STATE_FIND_BEACON,
	STATE_FIND_LINE,
	STATE_POSITION_ON_LINE,
	STATE_MOVE_BACKWARD,
	STATE_MOVE_FORWARD,
	STATE_WEDGE_MODE,
} States_t;

//Variable Definitions
States_t state_group;
States_t state;
uint8_t front_tape;
uint8_t mid_tape;
uint8_t left_tape;
uint8_t right_tape;
uint8_t back_tape;
int frontTapeReading;
int backTapeReading;
int leftTapeReading;
int midTapeReading;
int rightTapeReading;
int right_speed;
int left_speed;
static Metro driveTimer = Metro(3000);
static Metro wedgeTimer = Metro(5000);
uint8_t rightMotorForward;
uint8_t leftMotorForward;

// IR Variables
int print_freq = 100; // Hz
int target_IR_freq = 1000;
int target_freq_counter = 0;
int IRcounter = 0;
int IRcurrentfreq = 0;
int beacon_found = 0;

// Left Motor Direction Pins
int LeftIn1 = 9; //Forward if HIGH
int LeftIn2 = 10; //Forward if LOW

// Right Motor Direction Pins
int RightIn3 = 11; //Forward if HIGH
int RightIn4 = 12; //Forward if LOW

void setup()
{
	Serial.begin(9600);

	//Setup Motor Pins
	pinMode(RIGHT_MOTOR, OUTPUT);
	pinMode(LEFT_MOTOR, OUTPUT);
	pinMode(LeftIn1, OUTPUT);
	pinMode(LeftIn2, OUTPUT);
	pinMode(RightIn3, OUTPUT);
	pinMode(RightIn4, OUTPUT);
	pinMode(13, OUTPUT);
	digitalWrite(13, HIGH);

	//Setup Tape Sensor Pins
	pinMode(FRONT_TAPE_PIN, INPUT);
	pinMode(MID_TAPE_PIN, INPUT);
	pinMode(LEFT_TAPE_PIN, INPUT);
	pinMode(RIGHT_TAPE_PIN, INPUT);
	pinMode(BACK_TAPE_PIN, INPUT);

	state_group = STATE_PLANT_WEDGE;


	//setup for find beacon sate
	attachInterrupt(digitalPinToInterrupt(PIN_SIGNAL_IN), CountFallingEdges, FALLING);
	IRfreqTimer.begin(IRfreqRead,1000000/print_freq);
	digitalWrite(LeftIn1, LOW);
	digitalWrite(LeftIn2, HIGH);
	digitalWrite(RightIn3, HIGH);
	digitalWrite(RightIn4, LOW);
	rightMotorForward = 1;
	leftMotorForward = 0;
	analogWrite(RIGHT_MOTOR, MAX_SPEED);
	analogWrite(LEFT_MOTOR, MAX_SPEED);
	state = STATE_FIND_BEACON;
}

void loop()
{
	Serial.println(state);
	checkGlobalEvents();

	switch (state_group)
	{
	case STATE_PLANT_WEDGE:
		switch (state)
		{
		case STATE_FIND_BEACON:
			find_beacon();
			break;
		case STATE_FIND_LINE:
			find_line();
			break;
		case STATE_POSITION_ON_LINE:
			position_on_line();
			break;
		case STATE_MOVE_BACKWARD:
			move_backward();
			break;
		default:
			Serial.println("PLANT WEDGE, Wrong STATE");
			break;
			
		}
		break;
	case STATE_UNDERWAY:
		switch (state)
		{
		case STATE_FIND_LINE:
			find_line();
			break;
		case STATE_POSITION_ON_LINE:
			position_on_line();
			break;
		case STATE_MOVE_FORWARD:
			move_forward();
			break;
		case STATE_WEDGE_MODE:
			wedge_mode();
		default:
			Serial.println("UNDERWAY, Wrong STATE");
		}
	default: // Should never get into an unhandled state
		Serial.println("Wrong STATE_GROUP");
	}
}

void find_beacon(void)
{

if (true) //checkbeacon())  //if beacon found 
{
		digitalWrite(LeftIn1, HIGH);
		digitalWrite(LeftIn2, LOW);
		digitalWrite(RightIn3, HIGH);
		digitalWrite(RightIn4, LOW);
		rightMotorForward = 1;
		leftMotorForward = 1;

		analogWrite(RIGHT_MOTOR, MAX_SPEED);
		analogWrite(LEFT_MOTOR, MAX_SPEED);

		state = STATE_FIND_LINE;
}

}

void find_line(void)
{

// move forwards until mid tape sensor trips. If front and back sensors trip without mid, switch directions and reset sensors
	//Serial.println(analogRead(MID_TAPE_PIN));
	if (mid_tape) {
	//right motor forward, left motor backward, reset sensors, change state

		digitalWrite(LeftIn1, HIGH);
		digitalWrite(LeftIn2, LOW);
		digitalWrite(RightIn3, LOW);
		digitalWrite(RightIn4, HIGH);
		rightMotorForward = 1;
		leftMotorForward = 0;

		analogWrite(LEFT_MOTOR, SPIN_SPEED);
		analogWrite(RIGHT_MOTOR, SPIN_SPEED);
		resetTapeSensors();
		state = STATE_POSITION_ON_LINE;
	}
	else if (false)//front_tape && back_tape)
	{
		toggleRightMotorDirection();
		toggleLeftMotorDirection();
		resetTapeSensors();
	}
}

void position_on_line(void)
{
	// Spins counterclockwise until the center sensor finds the line.
	// When line is found go to drive forward state
	Serial.print("back_tape");
	Serial.println(back_tape);
	if (back_tape)
	{

		analogWrite(RIGHT_MOTOR, MAX_SPEED);
		analogWrite(LEFT_MOTOR, SLOWER_SPEED);
		driveTimer.reset();
		
		if (state_group == STATE_PLANT_WEDGE)
		{
			digitalWrite(LeftIn1, LOW);
			digitalWrite(LeftIn2, HIGH);
			digitalWrite(RightIn3, LOW);
			digitalWrite(RightIn4, HIGH);
			rightMotorForward = 0;
			leftMotorForward = 0;
			state = STATE_MOVE_BACKWARD;
		}
		else
		{
			digitalWrite(LeftIn1, HIGH);
			digitalWrite(LeftIn2, LOW);
			digitalWrite(RightIn3, HIGH);
			digitalWrite(RightIn4, LOW);
			rightMotorForward = 1;
			leftMotorForward = 1;
			state = STATE_MOVE_FORWARD;
		}
		
		
	}
	// else if (left_tape && right_tape)
	// {
	// 	toggleRightMotorDirection();
	// 	toggleLeftMotorDirection();
	// 	resetTapeSensors();
	// }
}
void move_backward(void)
{
	// Implements line following strategy while moving forward on top of the line.

	if (false) //if backwards contact sensor trips switch state
	{
		digitalWrite(LeftIn1, HIGH);
		digitalWrite(LeftIn2, LOW);
		digitalWrite(RightIn3, HIGH);
		digitalWrite(RightIn4, LOW);
		rightMotorForward = 1;
		leftMotorForward = 1;

		analogWrite(RIGHT_MOTOR, MAX_SPEED);
		analogWrite(LEFT_MOTOR, MAX_SPEED);
		front_tape = 1;
		state = STATE_FIND_LINE;
	}
	else if (left_tape)
	{
		analogWrite(RIGHT_MOTOR, MAX_SPEED);
		analogWrite(LEFT_MOTOR, SLOWER_SPEED);
	}
	else if (right_tape)
	{
		analogWrite(RIGHT_MOTOR, SLOWER_SPEED);
		analogWrite(LEFT_MOTOR, MAX_SPEED);
	}

}

void move_forward(void)
{
	// Implements line following strategy while moving forward on top of the line.

	if (driveTimer.check() && backwards_switch())
	{
		//  After driving forward for the driveTimer time, check if the bot
		//  is being pushed backwards, if so switch to wedge mode
		analogWrite(RIGHT_MOTOR, 0);
		analogWrite(LEFT_MOTOR, 0);
		wedgeTimer.reset();
		state = STATE_WEDGE_MODE;
	}

	else if (left_tape)
	{
		analogWrite(RIGHT_MOTOR, MAX_SPEED);
		analogWrite(LEFT_MOTOR, SLOWER_SPEED);
	}
	else if (right_tape)
	{
		analogWrite(RIGHT_MOTOR, SLOWER_SPEED);
		analogWrite(LEFT_MOTOR, MAX_SPEED);
	}
}

void wedge_mode(void)
{
	//  Waiting motionless for the enemy to leave. After the wedgeTimer expires go to the position on line state
	if (wedgeTimer.check())
	{

		digitalWrite(LeftIn1, LOW);
		digitalWrite(LeftIn2, HIGH);
		digitalWrite(RightIn3, HIGH);
		digitalWrite(RightIn4, LOW);
		rightMotorForward = 1;
		leftMotorForward = 0;

		analogWrite(RIGHT_MOTOR, MAX_SPEED);
		analogWrite(LEFT_MOTOR, MAX_SPEED);

		resetTapeSensors();
		state = STATE_POSITION_ON_LINE;
	}
}

void checkGlobalEvents(void)
{

	backwards_switch();
	checkFrontTape();
	checkMidTape();
	checkLeftTape();
	checkRightTape();
	checkBackTape();
}

int backwards_switch(void)
{
	//check whether the front limit switch is depressed. If depressed return 1. If not depressed return 0.
	return 1;
}

void checkFrontTape(void)
{
	//check the front tape sensor and set front_tape to 1 if tripped
	frontTapeReading = analogRead(FRONT_TAPE_PIN);
	if (frontTapeReading > LIGHT_THRESHOLD) front_tape = 1;
}

void checkMidTape(void)
{
	//check the mid tape sensor and set mid_tape to 1 if tripped
	
	midTapeReading = analogRead(MID_TAPE_PIN);
	if (midTapeReading > LIGHT_THRESHOLD)
	{ 
		mid_tape = 1;}
	//else mid_tape = 0;	
}

void checkLeftTape(void)
{
	//check the left tape sensor and set left_tape to 1 if tripped
	leftTapeReading = analogRead(LEFT_TAPE_PIN);
	if (leftTapeReading > LIGHT_THRESHOLD) left_tape = 1;
	else left_tape = 0;
}

void checkRightTape(void)
{
	//check the right tape sensor and set right_tape to 1 if tripped
	rightTapeReading = analogRead(RIGHT_TAPE_PIN);
	if (rightTapeReading > LIGHT_THRESHOLD) right_tape = 1;
	else right_tape = 0;
}

void checkBackTape(void)
{
	//check the back tape sensor and set back_tape to 1 if tripped
	backTapeReading = analogRead(BACK_TAPE_PIN);
	if (backTapeReading > LIGHT_THRESHOLD) back_tape = 1;
}

void resetTapeSensors(void)
{
	front_tape = 0;
	mid_tape = 0;
	left_tape = 0;
	right_tape = 0;
	back_tape = 0;
}

int checkbeacon(void)
{
	if (IRcurrentfreq > target_IR_freq -50 && IRcurrentfreq < target_IR_freq + 50){
     target_freq_counter ++;
   }
   else
   {
	   target_freq_counter = 0;
   }

   if (target_freq_counter > 100){
	   return 1;
   }
   else {
	   return 0;
   } 
}

void toggleRightMotorDirection()
{
	if (rightMotorForward)
	{
		digitalWrite(RightIn3, LOW);
		digitalWrite(RightIn4, HIGH);
		rightMotorForward = 0;
	}
	else
	{
		digitalWrite(RightIn3, HIGH);
		digitalWrite(RightIn4, LOW);
		rightMotorForward = 1;
	}
}

void toggleLeftMotorDirection()
{
	if (leftMotorForward)
	{
		digitalWrite(LeftIn1, LOW);
		digitalWrite(LeftIn2, HIGH);
		leftMotorForward = 0;
	}
	else
	{
		digitalWrite(LeftIn1, HIGH);
		digitalWrite(LeftIn2, LOW);
		leftMotorForward = 1;
	}
}

void CountFallingEdges(void){
  IRcounter ++;
}

void IRfreqRead(void){
	IRcurrentfreq = IRcounter*print_freq;
}
